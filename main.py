import uvicorn

from fastapi import FastAPI, Depends
from starlette.middleware.cors import CORSMiddleware

from core.database import engine, Base
from app.admin.routes import admin_router, bot_router
from core.auth.routes import auth_router
from app.kitchen.routes import kitchen_router
from app.gen.routes import gen_router
from app.driver.routes import driver_router
from app.sub.routes import sub_router
from app.admin.role_checker import RoleChecker


app = FastAPI(docs_url="/docs", redoc_url="/redocs")

Base.metadata.create_all(bind=engine)

"""  Including Main Routes  """
app.include_router(admin_router, prefix="/admin", tags=["admin-routes"], dependencies=[Depends(RoleChecker(["admin"]))])


"""  Including Auth Routes  """
app.include_router(auth_router, prefix="/auth", tags=["auth-routes"])


"""  Including Bot Routes  """
app.include_router(bot_router, prefix="/bot", tags=["bot-routes"])


"""  Including Kitchen Routes  """
app.include_router(kitchen_router, prefix="/kitchen", tags=["kitchen-routes"])


"""  Including Gen Routes  """
app.include_router(gen_router, prefix="/gen", tags=["gen-routes"], dependencies=[Depends(RoleChecker(["gen"]))])


"""  Including Driver Routes  """
app.include_router(driver_router, prefix="/driver", tags=["driver-routes"], dependencies=[Depends(RoleChecker(["driver"]))])


"""  Including Sub Routes  """
app.include_router(sub_router, prefix="/sub", tags=["sub-routes"], dependencies=[Depends(RoleChecker(["sub"]))])


# Set all CORS enabled origins
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


if __name__ == "__main__":
    Base.metadata.create_all(engine)
    uvicorn.run("main:app", host="127.0.0.1", port=7000)
