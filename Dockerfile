FROM python:3.8

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1
EXPOSE 7000
WORKDIR /app
COPY ./ /app
RUN pip install python-multipart PyJWT && pip install -r requirements.txt
CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "7000", "--log-config", "log.ini"]

