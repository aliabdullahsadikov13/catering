import datetime

from fastapi import FastAPI, Depends, HTTPException, status, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from core.database import SessionLocal, engine, Base
from core.auth import schemas as auth_schemas, auth, routes as auth_routes
from core import models
from app.gen import services, schemas
from app.admin import constants, services as admin_services, schemas as admin_schemas
from app.admin.routes import get_db


""" Initialisation Router Class """
gen_router = APIRouter()


@gen_router.get("/orders", status_code=200)
def get_gen_orders_list(date: Optional[datetime.date] = None, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Gen Contractor page
    """
    if not services.user_is_gen(current_user):
        raise HTTPException(status_code=403, detail="User has not Gen Contractor permissions")
    return services.get_gen_order_list_v3(db, current_user, date)


@gen_router.put("/orders/cencel", status_code=200)
def cencel_orders(ids: list, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Gen Contractor page
    """
    if not services.user_is_gen(current_user):
        raise HTTPException(status_code=403, detail="User has not Gen Contractor permissions")
    return admin_services.change_order_status(db, ids, constants.STATUS_CANCELLED)


@gen_router.put("/orders/approve", status_code=200)
def approve_orders(ids: list, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Gen Contractor page
    """
    return admin_services.change_order_status(db, ids, constants.STATUS_ALLOWED_BY_GEN)


@gen_router.get('/delivery/receive', status_code=200)
def receive_delivery(db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This api for the receive delivery from the driver
    """
    return services.receive_orders(db, current_user)


@gen_router.put('/delivery/cencel/{id}', status_code=200)
def cencel_delivery(id: int, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This api for the cencel delivery by gen contractor
    """
    cencel = services.cencel_or_approve_delivery(db, id, current_user, constants.STATUS_DELIVERY_CENCELLED)

    return cencel


@gen_router.put('/delivery/approve/{id}', status_code=200)
def approve_delivery(id: int, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This api for the approve delivery by gen contractor
    """
    approve = services.cencel_or_approve_delivery(db, id, current_user, constants.STATUS_DELIVERY_RECEIVED)

    return approve