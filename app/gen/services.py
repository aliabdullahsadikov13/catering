import json
from typing import List

from fastapi import HTTPException
from sqlalchemy.sql import text
import datetime

from core import models
from app.admin import schemas as admin_schemas, constants, services as admin_services, crud as admin_crud

def get_gen_order_list_x3(db, user, date):
    gen_objects = get_gen_objects(db, user.id)

    if date == None:
        date = datetime.date.today() + datetime.timedelta(days=1)

    p = []
    l = {}
    ids = {'id': []}
    for obj in gen_objects:
        query_body = text(f"""
            SELECT
                objects.id as object_id,
                objects.name as object_name,
                users.id as sub_id,
                users.firstname as sub_name,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                    
                ) as aup_breakfast,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as aup_lunch,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                    
                ) as aup_dinner,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as aup_night,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as foreman_breakfast,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as foreman_lunch,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as foreman_dinner,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as foreman_night,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as worker_breakfast,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as worker_lunch,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as worker_dinner,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{date}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{obj.id}'
                    AND status = 0
                    FETCH FIRST ROW ONLY
                ) as worker_night
            FROM ((object_sub_contractors
            INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
            INNER JOIN users ON object_sub_contractors.sub_contractor_id = users.id)
            WHERE objects.id = '{obj.id}'
            """)
        orders = db.execute(query_body)

        for o in orders:
            ids = {'id': []}
            order_ids = text(f"""
                        SELECT
                            orders.id
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{date}'
                        AND objects.id = '{obj.id}'
                        AND orders.sub_id = '{o['sub_id']}'
                        AND status = 0;
                        """)
            id_rows = db.execute(order_ids)
            for id in id_rows:
                ids['id'].append(id['id'])
            k = dict(o)
            ok = k.update(dict(ids))
            p.append(k)
    return p


def get_gen_order_list_v3(db, user, date):

    if date == None:
        date = datetime.date.today() + datetime.timedelta(days=1)
    p = []
    query_body = text(f"""
        SELECT
            obj.id as object_id,
            obj.name as object_name,
            users.id as sub_id,
            users.firstname as sub_name,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY

            ) as aup_breakfast,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as aup_lunch,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY

            ) as aup_dinner,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as aup_night,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_breakfast,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_lunch,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_dinner,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_night,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_breakfast,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_lunch,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_dinner,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_night,
            (
                SELECT array_agg(orders.id)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as ids
        FROM ((object_sub_contractors
        INNER JOIN objects obj ON object_sub_contractors.object_id = obj.id)
        INNER JOIN users ON object_sub_contractors.sub_contractor_id = users.id)
        WHERE obj.gen_contractor_id = '{user.id}'
        """)
    orders = db.execute(query_body)
    for o in orders:
        p.append(o)

    return p


def get_gen_order_list_demo(db, user, date):
    gen_objects = get_gen_objects(db, user.id)
    today = datetime.date.today() + datetime.timedelta(days=1)
    p = []
    for obj in gen_objects:
        query_body = text(f"""
                SELECT
                    orders.date AS date,
                    orders.id AS id,
                    objects.name AS object,
                    (SELECT users.firstname FROM users WHERE id = orders.sub_id) AS sub_name,
                    orders.sub_id AS sub_id,
                    (SELECT meal_types.meal_name_ru FROM meal_types WHERE id = orders.meal_type_id) AS meal_type,
                    (SELECT day_times.name_ru FROM day_times WHERE id = orders.day_time_id) AS day_time,
                    orders.amount AS amount,
                    orders.status AS status,
                    (SELECT clients.firstname FROM clients WHERE id = orders.client_id) AS client_name
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND objects.id = '{obj.id}'
                AND status = 0;
                """)
        orders = db.execute(query_body)

        for o in orders:
            p.append(o)
    data_dict = build_json(p)
    return len(list(p))


def get_gen_order_list_v1(db, user, date):
    gen_objects = get_gen_objects(db, user.id)
    """
    """
    if date == None:
        date = datetime.date.today() + datetime.timedelta(days=1)
    print(date)
    p = []
    for obj in gen_objects:
        query_body = text(f"""
                SELECT
                    orders.date AS date,
                    orders.id AS id,
                    objects.name AS object,
                    (SELECT users.firstname FROM users WHERE id = orders.sub_id) AS sub_name,
                    orders.sub_id AS sub_id,
                    (SELECT meal_types.meal_name_ru FROM meal_types WHERE id = orders.meal_type_id) AS meal_type,
                    (SELECT day_times.name_ru FROM day_times WHERE id = orders.day_time_id) AS day_time,
                    orders.amount AS amount,
                    orders.status AS status,
                    (SELECT clients.firstname FROM clients WHERE id = orders.client_id) AS client_name
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND objects.id = '{obj.id}'
                AND status = 0;
                """)
        orders = db.execute(query_body)

        for o in orders:
            p.append(o)
    data_dict = build_json(p)
    return data_dict


def build_json(data_rows):
    data = {"data": []}
    l = 0
    for d in data_rows:

        if (data["data"] != []) and (data["data"][l]["object"] == d.object):

            c = 0

            for sub in data["data"][l]["sub"]:
                if sub["name"] == d.sub_name:
                    i = 0
                    for meal_type in data["data"][l]["sub"][c]["meal_type"]:
                        print(data["data"][l]["sub"][c]["meal_type"])
                        print(i)
                        print(d.meal_type)
                        if data["data"][l]["sub"][c]["meal_type"][i]["name"] == d.meal_type:

                            if d.day_time not in list(data["data"][l]["sub"][c]["meal_type"][i]["day_times"].keys()):
                                print("jello")
                                data["data"][l]["sub"][c]["meal_type"][i]["day_times"][d.day_time] = d.amount
                            else:
                                print(d.day_time)
                                print(list(data["data"][l]["sub"][c]["meal_type"][i]["day_times"].keys()))
                        else:
                            print({d.day_time: d.amount})
                            data["data"][l]["sub"][c]["meal_type"].append({
                                "name": d.meal_type,
                                "day_times": {
                                    d.day_time: d.amount
                                }
                            })
                        i += 1
                else:
                    data["data"][l]["sub"].append({
                        "name": d.sub_name,
                        "id": d.sub_id,
                        "meal_type": [
                            {
                                "name": d.meal_type,
                                "day_times": {
                                    d.day_time: d.amount
                                }
                            }
                        ]
                    })
                c += 1
        else:
            data["data"].append({
                "date": d.date,
                "object": d.object,
                "sub": [
                    {
                        "name": d.sub_name,
                        "id": d.sub_id,
                        "meal_type": [
                            {
                                "name": d.meal_type,
                                "day_times": {
                                    d.day_time: d.amount
                                }
                            }
                        ]
                    }
                ]
        })
        l += 1
    return data


def get_gen_order_list(db, user):
    gen_objects = get_gen_objects(db, user.id)
    return gen_objects


def user_is_gen(user):
    if (user.role_id == 1) and (user.status == 1):
        return True
    return False


def get_gen_objects(db, user_id) -> admin_schemas.Object:
    objs = db.query(models.Object)\
        .filter(models.Object.gen_contractor_id == user_id)\
        .all()

    return objs

#
# def cencel_orders(db, ids):
#     failed = []
#     for id in ids:
#         order_model = db.query(models.Order).filter(models.Order.id == id).first()
#         if order_model:
#             order_model.status = constants.STATUS_CANCELLED
#             db.add(order_model)
#             db.commit()
#             db.refresh(order_model)
#         else:
#             failed.append(id)
#     return failed if failed != [] else True
#
#
# def approve_orders(db, ids):
#     failed = []
#     for id in ids:
#         order_model = db.query(models.Order).filter(models.Order.id == id).first()
#         if order_model:
#             order_model.status = constants.STATUS_ALLOWED_BY_GEN
#             db.add(order_model)
#             db.commit()
#             db.refresh(order_model)
#         else:
#             failed.append(id)
#
#     return failed if failed != [] else True




"""
data structure for kitchen table in UI
data {
    date: 00.00.0000,
    object: Akfa,
    body: [
        {
            day_time: day_time_name, # breakfast
            day_time_id: id,
            meal_types: {
                order_ids: [],
                content: {
                    aup: 25,
                    manager: 55,
                    staff: 125
                }
            }
        },
        {
            day_time: day_time_name, # lunch
            day_time_id: id,
            meal_types: [
                {
                    order_ids: [],
                    content: {
                        aup: 25,
                        manager: 55,
                        staff: 125
                    }  
                },
            ]
        },
        {
            day_time: day_time_name, # dinner
            day_time_id: id,
            meal_types: [
                {
                    order_ids: [],
                    content: {
                        aup: 25,
                        manager: 55,
                        staff: 125
                    }  
                },
            ]
        },
        {
            day_time: day_time_name, # night
            day_time_id: id,
            meal_types: [
                {
                    order_ids: [],
                    content: {
                        aup: 25,
                        manager: 55,
                        staff: 125
                    }  
                },
            ]
        },
    ]
}

"""


def receive_orders(db, user):
    today = datetime.date.today()
    deliveries = db.query(models.Delivery)\
        .join(models.Object, models.Object.id == models.Delivery.object_id)\
        .filter(models.Object.gen_contractor_id == user.id,
                models.Delivery.date == today,
                models.Delivery.status == 0)\
        .all()
    data = []
    if deliveries:
        for delivery in deliveries:
            user_driver = admin_crud.get_user_by_id(db, delivery.driver_id)
            data.append({
                "id": delivery.id,
                "date": delivery.date,
                "driver": user_driver.firstname + ' ' + user_driver.lastname,
                "car_number": delivery.car_number,
                "meal_type": delivery.meal_type,
                "order_ids": delivery.order_ids,
                "meal_amounts_json": json.loads(delivery.meal_amounts_json.replace("'", "\"").replace("None", "null"))
            })

        return data
    return []


# def check_delivery_orders_is_actual(db, order_ids):
#     orders = db.query(models.Order).filter(models.Order.id in order_ids).all()
#     if orders:
#         for order in orders:
#             order.status = constants.STATUS_SHIPPING
#             db.add(order)
#             db.commit()


def cencel_or_approve_delivery(db, id, user, delivery_status):
    today = datetime.date.today()
    delivery = db.query(models.Delivery) \
        .filter(models.Delivery.id == id,
                models.Delivery.status == 0,
                models.Delivery.date == today) \
        .first()

    """ change status """
    if delivery:
        delivery.status = delivery_status
        db.add(delivery)
        db.commit()
        db.refresh(delivery)

        changed_orders = False
        status = False
        if (delivery.status == delivery_status) and (delivery_status == constants.STATUS_DELIVERY_CENCELLED):
            status = delivery_status
            changed_orders = admin_services.change_order_status(db, delivery.order_ids, delivery_status)

        elif (delivery.status == delivery_status) and (delivery_status == constants.STATUS_DELIVERY_RECEIVED):
            status = constants.STATUS_COMPLETED
            changed_orders = admin_services.change_order_status(db, delivery.order_ids, constants.STATUS_COMPLETED)

        if isinstance(changed_orders, list):
            orders = db.query(models.Order).filter(models.Order.id in changed_orders).all()
            if orders and status:
                for order in orders:
                    order.status = status
                    db.add(order)
                    db.commit()
            return HTTPException(status_code=200, detail="Orders successfuly cencelled")
        elif changed_orders:
            return HTTPException(status_code=200, detail="All orders successfuly approved")
    else:
        raise HTTPException(status_code=404, detail="Delivery has not found")