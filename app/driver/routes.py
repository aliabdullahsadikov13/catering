import os
import shutil

from fastapi import FastAPI, Depends, HTTPException, status, APIRouter, UploadFile, File, Form
from pydantic import constr
from sqlalchemy.orm import Session
from typing import List, Optional

from core.database import SessionLocal, engine, Base
# from app.admin import schemas, contstants
from app.admin import services as admin_services
from app.admin import constants
from app.driver import schemas, services
from core import models
from core.auth import routes as auth_routes, schemas as auth_schemas, auth
from app.admin.routes import get_db


""" Initialisation Router Class """
driver_router = APIRouter()


@driver_router.get('/orders')
def get_orders(db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This API for the shipping,
    the driver will take completed orders from the kitchen,
    for the shipping to client.
    """
    return services.get_driver_orders(db, current_user)


@driver_router.put("/orders/approve", status_code=200)
def approve_orders(ids: list, data: dict, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This api for the update orders status to status shipping
    """
    return services.make_delivery(db, ids, data, current_user)


@driver_router.post('/defect')
async def create_defect(
        delivery_id: int = Form(...),
        amount: int = Form(...),
        reason: str = Form(...),
        comment: str = Form(...),
        files: List[UploadFile] = File(...),
        db: Session = Depends(get_db),
        current_user: models.User = Depends(auth_routes.get_current_user)
):
    """
    Handle defect photos end data of defect
    """

    data = {
        "delivery_id": delivery_id,
        "amount": amount,
        "reason": reason,
        "comment": comment,
        "files": files,
    }
    save = services.save_defect(db, data, current_user)
    return save


