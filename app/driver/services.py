import datetime, time
import json
import os
import sys
import shutil
from typing import List

from fastapi import HTTPException, Body
from sqlalchemy import text

from core import models
from app.admin import constants, schemas as admin_schemas, services as admin_services, constants
from app.driver import schemas


def get_driver_orders(db, user):
    today = datetime.date.today()

    driver_order_query = text(f"""
        SELECT
            objects.id,
            objects.name,
            (
                SELECT day_times.name
                FROM day_times
                WHERE o.day_time_id = day_times.id
                ORDER BY day_times.name
            ) as day_time_name,

            (
                SELECT SUM(orders.amount) as amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND status = 2
                AND orders.meal_type_id = 1
                AND orders.day_time_id = o.day_time_id
                AND objects.driver_id = '{user.id}'
                GROUP BY objects.id, orders.day_time_id
            ) as aup,
            
            (
                SELECT SUM(orders.amount) as amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND status = 2
                AND orders.meal_type_id = 2
                AND orders.day_time_id = o.day_time_id
                AND objects.driver_id = '{user.id}'
                GROUP BY objects.id, orders.day_time_id
            ) as foreman,
            
            (
                SELECT SUM(orders.amount) as amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND status = 2
                AND orders.meal_type_id = 3
                AND orders.day_time_id = o.day_time_id
                AND objects.driver_id = '{user.id}'
                GROUP BY objects.id, orders.day_time_id
            ) as worker,
            array_agg(o.id) as ids
        FROM ((object_sub_contractors
        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
        INNER JOIN orders o ON object_sub_contractors.sub_contractor_id = o.sub_id)
        WHERE o.date = '{today}'
        AND status = 2
        AND objects.driver_id = '{user.id}'
        GROUP BY objects.id, o.day_time_id;
    """)

    driver_orders_row = db.execute(driver_order_query)
    data = []
    for order in driver_orders_row:
        data.append(order)

    return data


def user_is_driver(user):
    if (user.role_id == 4) and (user.status == 1):
        return True
    return False


def make_delivery(db, ids, data, user) -> admin_schemas.Delivery:

    """ checking to defend dublicate saving """
    if 'day_time_name' not in data:
        raise HTTPException(status_code=401, detail="Parameter error")

    earlier_saved = db.query(models.Delivery) \
        .filter(models.Delivery.meal_type == data['day_time_name'],
                models.Delivery.date == datetime.date.today(),
                models.Delivery.status == 0) \
        .first()
    if earlier_saved and (id in earlier_saved.order_ids for id in ids):
        raise HTTPException(status_code=403, detail="The delivery created with this ids")

    change_status = False
    if not ids:
        raise HTTPException(status_code=401, detail="ids is empty")

    change_status = admin_services.change_order_status(db, ids, constants.STATUS_SHIPPING, user)

    if change_status:
        return create_delivery(db, ids, data, user)
    raise HTTPException(status_code=500, detail="Operation error in make_delivery function")


def create_delivery(db, ids, data, user):
    driver_car = get_driver_car(db, user)
    try:
        delivery = models.Delivery(
            driver_id=user.id,
            object_id=data['id'],
            car_number=driver_car.car_number,
            meal_type=data['day_time_name'],
            date=datetime.date.today(),
            meal_amounts_json=str(data),
            order_ids=ids
        )
        db.add(delivery)
        db.commit()
        db.refresh(delivery)

        return delivery
    except HTTPException:
        raise HTTPException(status_code=500, detail="Delivery has not created successfuly")


def get_driver_car(db, user) -> admin_schemas.CarBase:
    car = db.query(models.DriverCar, models.Car)\
        .join(models.Car, models.Car.id == models.DriverCar.car_id)\
        .join(models.User, models.User.id == models.DriverCar.driver_id)\
        .filter(models.User.role_id == 4, models.DriverCar.driver_id == user.id)\
        .first()
    if car:
        data = {
            'car_number': car.Car.car_number,
            'model': car.Car.model
        }
    else:
        raise HTTPException(status_code=404, detail="Car not found")

    return admin_schemas.CarBase(**data)


UPLOAD_FILE_DEFECTS = '/uploads/defects/'


def save_defect(db, data, user):
    is_exist = db.query(models.Delivery).filter(models.Delivery.id == data['delivery_id']).first()
    if is_exist:
        check_defect = db.query(models.Defect).filter(models.Defect.delivery_id == data['delivery_id']).first()
        if check_defect:
            raise HTTPException(status_code=403, detail="You can't send more than one defect to same delivery")
        defect_model = models.Defect(
            delivery_id=data['delivery_id'],
            amount=data['amount'],
            reason=data['reason'],
            comment=data['comment'],
        )
        db.add(defect_model)
        db.commit()
        db.refresh(defect_model)

        for file in data['files']:
            path = os.path.dirname(os.path.abspath("uploads/defects/"))
            with open(f'{path+"/defects/"+file.filename}', "wb") as defect_img:
                shutil.copyfileobj(file.file, defect_img)

            # Create Defect photos
            photo = models.DefectReason(
                defect_id = defect_model.id,
                photo = f"/uploads/defects/{file.filename}"
            )
            db.add(photo)
            db.commit()
            db.refresh(photo)

        return HTTPException(status_code=200, detail="Defects saved")
    raise HTTPException(status_code=404, detail="Delivery not found!")
