import datetime

from typing import List, Optional

from fastapi import File
from pydantic import BaseModel


class Defect(BaseModel):
    delivery_id: int
    amount: int
    reason: str
    comment: str


