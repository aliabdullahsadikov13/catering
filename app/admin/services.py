import datetime, time
import json
import os
import shutil
from typing import List, Optional

from fastapi import HTTPException, Body
from sqlalchemy import text, inspect

from core import models
from app.admin import schemas, constants
from app.admin import crud as admin_crud
from core.auth import schemas as auth_schemas


def client_exist(db, phone_number) -> bool:
    client = db.query(models.Client).filter(models.Client.phone_number == phone_number).first()
    return True if client else False


def approved_or_cencelled(db, phone_number):
    today = datetime.date.today() + datetime.timedelta(days=1)
    client = db.query(models.Client).filter(models.Client.phone_number == phone_number).first()
    data = {}
    if client:
        orders = db.query(models.Order)\
            .filter(models.Order.date == today,
                    models.Order.status == 1,
                    models.Order.client_id == client.id)\
            .all()
        if orders and (len(orders) > 3):
            data = {
                "status_code": 200,
                "status": 1,
                "message": "Order approved by Gen Contractor"
            }
        else:
            orders = db.query(models.Order) \
                .filter(models.Order.date == today,
                        models.Order.status == -1,
                        models.Order.client_id == client.id) \
                .all()
            if orders:
                data = {
                    "status_code": 200,
                    "status": -1,
                    "message": "Order cencelled by Gen Contractor"
                }
    return data


def get_client(db, phone_number: str):
    client = db.query(models.ObjectSubContractor, models.Client, models.Object)\
        .join(models.Client, models.Client.sub_contractor_id == models.ObjectSubContractor.sub_contractor_id)\
        .join(models.Object, models.Object.id == models.ObjectSubContractor.object_id)\
        .filter(models.Client.phone_number == phone_number).first()
    data = {
        "id": client.Client.id,
        "sub_contractor_id": client.Client.sub_contractor_id,
        "phone_number": client.Client.phone_number,
        "firstname": client.Client.firstname,
        "object_name": client.Object.name
    }
    return data


def save_orders(db, order_data: schemas.OrderCreate):
    client = None
    if order_data.client_phone:
        "get client"
        client = get_client(db, order_data.client_phone)

    if client:
        try:
            for order in order_data.orders:
                admin_crud.create_order(db, order, client=client)
        except HTTPException:
            raise HTTPException(status_code=500, detail="Order does not saved")
    elif order_data.sub_id:
        try:
            for order in order_data.orders:
                admin_crud.create_order(db, order, sub_id=order_data.sub_id)
        except HTTPException:
            raise HTTPException(status_code=500, detail="Order does not saved")
    else:
        raise HTTPException(status_code=401, detail="Client phone or Sub contractor ID must be selected")




    return True


def save_new_orders(db, order_data: schemas.OrderCreate):
    client = None
    if order_data.client_phone:
        # get client
        client = get_client(db, order_data.client_phone)

    if client:
        try:
            for order in order_data.orders:
                admin_crud.create_order(db, order, client=client)
        except HTTPException:
            raise HTTPException(status_code=500, detail="Order does not saved")
    elif order_data.sub_id:
        try:
            for order in order_data.orders:
                admin_crud.create_order(db, order, sub_id=order_data.sub_id)
        except HTTPException:
            raise HTTPException(status_code=500, detail="Order does not saved")
    else:
        raise HTTPException(status_code=401, detail="Client phone or Sub contractor ID must be selected")




    return True


def change_order_status(db, ids, status, user: Optional[auth_schemas.UserInDb] = None):
    failed = []
    for id in ids:
        id = int(id)
        if not isinstance(id, int):
            raise HTTPException(status_code=401, detail="Content of list must be integer")
        order_model = db.query(models.Order).filter(models.Order.id == id).first()
        if order_model:
            order_model.status = status
            order_model.driver_id = user.id if status == constants.STATUS_SHIPPING else None
            db.add(order_model)
            db.commit()
            db.refresh(order_model)
        else:
            failed.append(id)

    return HTTPException(status_code=401, detail=f"This ids {failed} of orders has not saccessfuly cencelled") if failed != [] else HTTPException(status_code=200, detail="Orders successfuly cencelled")


def user_exist(db, username, user_id: Optional[int] = None) -> bool:
    if user_id:
        user = db.query(models.User).filter(models.User.username == username, models.User.id != user_id).first()
        return True if user else False

    user = db.query(models.User).filter(models.User.username == username).first()
    return True if user else False


def gen_exist(db, gen_id):
    gen_model = db.query(models.User).filter(models.User.role_id == 1, models.User.id == gen_id).first()

    return gen_model


def sub_exist(db, sub_id):
    sub_model = db.query(models.User).filter(models.User.role_id == 2, models.User.id == sub_id).first()

    return sub_model


def kitchen_exist(db, kitchen_id):
    kitchen_model = db.query(models.User).filter(models.User.role_id == 3, models.User.id == kitchen_id).first()

    return kitchen_model


def driver_exist(db, driver_id):
    driver_model = db.query(models.User).filter(models.User.role_id == 4, models.User.id == driver_id).first()

    return driver_model


def object_exist(db, name):
    object_model = db.query(models.Object).filter(models.Object.name == name).first()

    return object_model


def get_role_name(db, role_id):
    role = db.query(models.Role).filter(models.Role.id == role_id).first()
    return role.role_name


def change_user_status(db, status, user_id):
    user = db.query(models.User).filter(models.User.id == user_id).first()
    if user:
        user.status = status
        db.add(user)
        db.commit()
        return True
    return False


def save_file(file):
    try:
        path = os.path.dirname(os.path.abspath("uploads/users/"))
        with open(f'{path + "/users/" + file.filename}', "wb") as img:
            shutil.copyfileobj(file.file, img)
    except HTTPException:
        raise HTTPException(status_code=500, detail="The image has not saved")


def object_as_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


def fio_separator(fio):
    f_i_o = fio.split(" ")
    if len(f_i_o) > 1:
        firstname = f_i_o[0]
        lastname = f_i_o[1]
    else:
        firstname = fio
        lastname = ''

    return {"firstname": firstname, "lastname": lastname}