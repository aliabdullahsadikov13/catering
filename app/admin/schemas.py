import datetime
from typing import List, Optional

from fastapi import Form, Query
from pydantic import BaseModel, constr

from core.auth import schemas



""" Role """


class Role(BaseModel):
    id: int
    role_name: str


class RoleCreate(BaseModel):
    role_name: str


class UserWithRoleName(schemas.UserInDb):
    role: Role


""" Object """


class ObjectCreate(BaseModel):
    name: str
    address: str
    driver_id: int
    gen_contractor_id: int
    kitchen_id: int


class ObjectCreateForm(BaseModel):
    name: str = Form(...)
    address: str = Form(...)
    driver_id: int = Query('lo', enum=['l','p'])
    gen_contractor_id: int = Form(...)
    kitchen_id: int = Form(...)


class Object(ObjectCreate):
    id: int


class ObjectSubGroupCreate(BaseModel):
    object_id: int
    sub_contractor_id: int


class ObjectSubGroup(ObjectSubGroupCreate):
    id: int


""" Car """


class CarBase(BaseModel):
    car_number: str
    model: str


class CarCreate(CarBase):
    kitchen_id: int


class Car(CarCreate):
    id: int


""" Defect """


class DefectCreate(BaseModel):
    order: str
    amount: int
    reason_id: int
    comment: str


class Defect(ObjectCreate):
    id: int




""" Client """


class ClientInDb(BaseModel):
    id: int
    sub_contractor_id: int
    firstname: str
    lastname: str
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)


class ClientCreate(BaseModel):
    firstname: str
    lastname: str
    sub_contractor_id: int
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)


class ClientCheck(BaseModel):
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)


""" ORDER """


class OrderBody(BaseModel):
    meal_type_id: int
    day_time_id: int
    amount: int


class OrderCreate(BaseModel):
    client_phone: Optional[str] = None
    sub_id: Optional[int] = None
    orders: List[OrderBody]


class DayTimeCreate(BaseModel):
    name: str
    name_uz: str
    name_ru: str
    active: bool = True


class MealTypeCreate(BaseModel):
    meal_name: str
    meal_name_uz: str
    meal_name_ru: str
    active: bool = True


""" Delivery """


class DeliveryBase(BaseModel):
    driver_id: int
    objects_id: int
    car_number: str
    meal_amounts_json: str
    order_ids: list

    class Config:
        orm_mode = True

class DeliveryCreate(DeliveryBase):
    status: int
    created_at: datetime.datetime


class Delivery(DeliveryBase):
    id: int


class DeliveryUpdate(Delivery):
    updated_at: datetime.datetime