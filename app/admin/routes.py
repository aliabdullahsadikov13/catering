import enum
import os
import shutil
from datetime import datetime

from fastapi import FastAPI, Depends, HTTPException, status, APIRouter, Query, Form, UploadFile, File
from pydantic import constr
from sqlalchemy.orm import Session
from typing import List, Optional

from core.database import SessionLocal, engine, Base
from app.admin import schemas, constants
from app.admin import services as admin_services
from app.admin import crud as admin_crud
from core.auth import schemas as auth_schemas, auth
from core import models


""" Initialisation Router Class """
admin_router = APIRouter()


def get_db():
    """ Get Session """
    db = ''
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


@admin_router.get('/')
def home_page():
    return {"What's uuup meen!"}


""" User """


@admin_router.get('/users', status_code=200)
def list_users(role_name: str = Query('eu', enum=(['admin','gen','sub','kitchen', 'driver', 'sub_office']), description='foo bar'), db: Session = Depends(get_db)):
    """
    Get Users List
    :param role_name:
    :param db:
    :return:
    """
    return admin_crud.get_user_by_role_name_v2(db, role_name)


@admin_router.post("/gen/create", response_model=auth_schemas.GenOrKitchenInDb)
def gen_create(
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_GEN).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.GenOrKitchenCreate(
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.create_gen_or_kitchen(db=db, user=user)


@admin_router.patch("/gen/update", response_model=auth_schemas.GenOrKitchenInDb)
def gen_update(
        id: int,
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_GEN).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.GenOrKitchenUpdate(
        id=id,
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.update_gen_or_kitchen(db=db, user=user)


@admin_router.post("/sub/create", response_model=auth_schemas.GenOrKitchenInDb)
def sub_create(
        fio: Optional[str] = Form(...),
        object_id: int = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_SUB).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.GenOrKitchenCreate(
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.create_gen_or_kitchen(db=db, user=user, object_id=object_id)


@admin_router.patch("/sub/update", response_model=auth_schemas.GenOrKitchenInDb)
def sub_update(
        id: int,
        fio: Optional[str] = Form(...),
        object_id: int = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_SUB).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    """ Update Object Sub model """
    object = db.query(models.Object).filter(models.Object.id == object_id).first()
    if object:
        obj_sub_model = db.query(models.ObjectSubContractor).filter(models.ObjectSubContractor.sub_contractor_id == id).first()
        if obj_sub_model:
            obj_sub_model.object_id = object_id

            db.add(obj_sub_model)
            db.commit()
        else:
            obj_sub_new = models.ObjectSubContractor(object_id=object_id, sub_contractor_id=id)
            db.add(obj_sub_new)
            db.commit()
    else:
        raise HTTPException(status_code=404, detail="Object has not found")

    user = auth_schemas.GenOrKitchenUpdate(
        id=id,
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.update_gen_or_kitchen(db=db, user=user)


@admin_router.post("/kitchen/create", response_model=auth_schemas.GenOrKitchenInDb)
def kitchen_create(
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_KITCHEN).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.GenOrKitchenCreate(
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.create_gen_or_kitchen(db=db, user=user)


@admin_router.patch("/kitchen/update", response_model=auth_schemas.GenOrKitchenInDb)
def kitchen_update(
        id: int,
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        address: Optional[str] = Form(None),
        email: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_KITCHEN).first()

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.GenOrKitchenUpdate(
        id=id,
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
        address=address,
        email=email
    )

    return auth.update_gen_or_kitchen(db=db, user=user)


@admin_router.post("/driver/create")
def driver_create(
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        car_id: int = Form(...),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == constants.ROLE_DRIVER).first()

    """ Check car exsist """
    car = db.query(models.Car).filter(models.Car.id == car_id).first()
    if not car:
        raise HTTPException(status_code=404, detail="Car not found")

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.DriverCreate(
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        car_id=car_id,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
    )

    return auth.create_driver(db=db, user=user)


@admin_router.patch("/driver/update")
def driver_update(
        id: int = Form(...),
        fio: Optional[str] = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        username: str = Form(...),
        password: str = Form(...),
        car_id: int = Form(...),
        img_file: UploadFile = File(None),
        db: Session = Depends(get_db)
):
    """ Check car exsist """
    car = db.query(models.Car).filter(models.Car.id == car_id).first()
    if not car:
        raise HTTPException(status_code=404, detail="Car not found")

    """ FIO separate """
    name = admin_services.fio_separator(fio)

    """ File Save """
    if img_file:
        admin_services.save_file(img_file)
        file_path_name = f"/uploads/users/{img_file.filename.replace(' ', '')}"
    else:
        file_path_name = constants.DEFAULT_IMAGE

    user = auth_schemas.DriverUpdate(
        id=id,
        username=username,
        password=password,
        phone_number=phone_number,
        car_id=car_id,
        photo=file_path_name,
        firstname=name['firstname'],
        lastname=name['lastname'],
    )

    return auth.update_driver(db=db, user=user)


@admin_router.patch("/user/disactivate/{id}")
def user_disactive(id: int, db: Session = Depends(get_db)):
    return admin_services.change_user_status(db, constants.USER_STATUS_DISACTIVE, id)


@admin_router.patch("/user/activate/{id}")
def user_active(id: int, db: Session = Depends(get_db)):
    return admin_services.change_user_status(db, constants.USER_STATUS_ACTIVE, id)


""" Role """


@admin_router.get('/roles', status_code=200)
def role_list(db: Session = Depends(get_db)):
    """
    Role list
    :param db:
    :return:
    """
    return admin_crud.get_role(db)


@admin_router.post('/roles', status_code=200)
def create_role(data: schemas.RoleCreate, db: Session = Depends(get_db)):
    """
    Create Role
    :param data:
    :param db:
    :return:
    """
    return admin_crud.create_role(db, data)


@admin_router.put('/roles', status_code=200)
def update_role(data: schemas.Role, db: Session = Depends(get_db)):
    """
    Update Role
    :param data:
    :param db:
    :return:
    """
    return admin_crud.update_role(db, data)


""" Object """


@admin_router.get('/object', status_code=200)
def object_list(db: Session = Depends(get_db)):
    """
    Object list
    :param db:
    :return:
    """
    return admin_crud.get_object(db)


# @admin_router.post('/object', status_code=200)
# def object_create(data: schemas.ObjectCreateForm, db: Session = Depends(get_db)):
#     """
#     Create Object
#     :param data:
#     :param db:
#     :return:
#     """
#     return admin_crud.create_object(db, data)

@admin_router.post('/object', status_code=200)
def object_create(data: schemas.ObjectCreateForm = Depends(), db: Session = Depends(get_db)):
    """
    Create Object
    :param data:
    :param db:
    :return:
    """
    return admin_crud.create_object(db, data)


@admin_router.put('/object', status_code=200)
def object_update(data: schemas.Object, db: Session = Depends(get_db)):
    """
    Update Object
    :param data:
    :param db:
    :return:
    """
    return admin_crud.update_object(db, data)


@admin_router.post('/object_sub_group', status_code=200)
def object_sub_group_create(data: schemas.ObjectSubGroupCreate, db: Session = Depends(get_db)):
    """
    Create Object Sub Group
    :param data:
    :param db:
    :return:
    """
    return admin_crud.create_object_sub_group(db, data)


""" Car """


@admin_router.get('/car', status_code=200)
def car_list(db: Session = Depends(get_db)):
    """
    Car list
    :param db:
    :return:
    """
    return admin_crud.get_car(db)


@admin_router.post('/car', status_code=200)
def car_create(data: schemas.CarCreate, db: Session = Depends(get_db)):
    """
    Create Car
    :param data:
    :param db:
    :return:
    """
    return admin_crud.create_car(db, data)


@admin_router.put('/car', status_code=200)
def car_update(data: schemas.Car, db: Session = Depends(get_db)):
    """
    Update Car
    :param data:
    :param db:
    :return:
    """
    return admin_crud.update_car(db, data)


""" Defects """


@admin_router.get('/defect', status_code=200)
def defect_list(db: Session = Depends(get_db)):
    """
    Defect list
    :param db:
    :return:
    """
    return admin_crud.get_defect(db)
#
#
# @admin_router.post('/defect', status_code=200)
# def defect_create(data: schemas.CarCreate, db: Session = Depends(get_db)):
#     """
#     Create Defect
#     :param data:
#     :param db:
#     :return:
#     """
#     return admin_crud.create_defect(db, data)
#
#
# @admin_router.put('/defect', status_code=200)
# def defect_update(data: schemas.Object, db: Session = Depends(get_db)):
#     """
#     Update Defect
#     :param data:
#     :param db:
#     :return:
#     """
#     return admin_crud.update_defect(db, data)


""" Bot Routes """


bot_router = APIRouter()


@bot_router.get('/check_client/{phone_number}')
def check_client(phone_number, db: Session = Depends(get_db)):
    """
    This route for checking client witch deside to order foods by telegram bot, get phone number and make check
    :param phone_number:
    :return:
    """
    if admin_services.client_exist(db, phone_number):
        return admin_services.get_client(db, phone_number)
    raise HTTPException(status_code=403, detail="Client with this phone number has not exist")


@bot_router.post('/client/save_order', status_code=200)
def save_order(order_data: schemas.OrderCreate, db: Session = Depends(get_db)):
    """
    We will get data of order from client if all of the fields is valid than we will make save it
    :param order_data:
    :return:
    """
    if not admin_services.client_exist(db, order_data.client_phone) and order_data.sub_id == 0:
        raise HTTPException(status_code=403, detail="Client with this phone number has not exist")
    elif (not admin_services.client_exist(db, order_data.client_phone)) and (not admin_services.sub_exist(db, order_data.sub_id)):
        raise HTTPException(status_code=403, detail="Sub Contractor and Client has not exist")

    save_order = admin_services.save_orders(db, order_data)

    return True


@bot_router.post('/client/save_neworder', status_code=200)
def save_order(order_data: schemas.OrderCreate, db: Session = Depends(get_db)):
    """
    This api for saving orders with another method as nosql way
    :param order_data:
    :return:
    """
    if not admin_services.client_exist(db, order_data.client_phone) and order_data.sub_id == 0:
        raise HTTPException(status_code=403, detail="Client with this phone number has not exist")
    elif (not admin_services.client_exist(db, order_data.client_phone)) and (not admin_services.sub_exist(db, order_data.sub_id)):
        raise HTTPException(status_code=403, detail="Sub Contractor and Client has not exist")

    save_order = admin_services.save_orders(db, order_data)

    return True



@bot_router.get("/meal_type")
def meal_type_list(db: Session = Depends(get_db)):
    d = admin_crud.meal_type_list(db)
    lang = ['uz', 'ru']
    item = [{'uz': [], 'ru': []}]
    for lang_code in lang:
        for data in d:
            item[0][lang_code].append({"id": data.id, "name": data.meal_name_uz if lang_code == 'uz' else data.meal_name_ru,
                                       "callback_name": data.meal_name})
    return item


@bot_router.get("/day_time")
def day_time_list(db: Session = Depends(get_db)):
    d = admin_crud.day_time_list(db)
    lang = ['uz', 'ru']
    item = [{'uz': [], 'ru': []}]
    for lang_code in lang:
        for data in d:
            item[0][lang_code].append({"id": data.id, "name": data.name_uz if lang_code == 'uz' else data.name_ru, "callback_name": data.name})
    return item


@bot_router.get('/check/approved/{phone_number}')
def get_approved(phone_number, db: Session = Depends(get_db)):
    """
    This route for checking what Gen Contractor approved or cencelled
    :param phone_number:
    :return:
    """
    if admin_services.client_exist(db, phone_number):
        return admin_services.approved_or_cencelled(db, phone_number)
    raise HTTPException(status_code=403, detail="Client with this phone number has not exist")


""" Bot Routes End """

""" Client Routes """


@admin_router.get("/client", status_code=200)
def list_client(db: Session = Depends(get_db)):
    """
    Get list of clients
    :param db:
    :return:
    """
    return admin_crud.get_list(db)


@admin_router.post('/client/create', status_code=201)
def create_client(client: schemas.ClientCreate, db: Session = Depends(get_db)):
    """
    Create client by admin panel
    :param client:
    :param db:
    :return:
    """
    if admin_services.client_exist(db, client.phone_number):
        raise HTTPException(status_code=403, detail="Client with this phone number already exist")
    return admin_crud.create_client(db, client)


@admin_router.patch('/client/{id}', status_code=200)
def update_client(id: int, client: schemas.ClientCreate, db: Session = Depends(get_db)):
    """
    Update client by admin panel
    :param client:
    :param db:
    :return:
    """
    return admin_crud.update_client(db, id, client)


""" Client Routes """

""" Date Time """


@admin_router.post("/day_time/create", status_code=200)
def create_day_time(day_time_data: schemas.DayTimeCreate, db: Session = Depends(get_db)):
    """
    For additional table day_time
    :param db:
    :param day_time_data:
    :return:
    """
    return admin_crud.create_day_time(db, day_time_data)


@admin_router.post("/meal_type/create", status_code=200)
def create_day_time(meal_type_data: schemas.MealTypeCreate, db: Session = Depends(get_db)):
    """
    For additional table meal_type
    :param db:
    :param meal_type_data:
    :return:
    """
    return admin_crud.create_meal_type(db, meal_type_data)

