from typing import List

from fastapi import Depends, HTTPException
from fastapi.logger import logger
from sqlalchemy.orm import Session

from core.models import User
from core.auth import routes as auth_routes
from app.admin import services as admin_services
from core.database import SessionLocal, engine, Base


def get_db():
    """ Get Session """
    db = ''
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


class RoleChecker:

    def __init__(self, allowed_roles: List):
        self.allowed_roles = allowed_roles

    def __call__(self, user: User = Depends(auth_routes.get_current_user), db: Session = Depends(get_db)):
        role_name = admin_services.get_role_name(db, user.role_id)
        if role_name not in self.allowed_roles:
            logger.debug(f"User with role {role_name} not in {self.allowed_roles}")
            raise HTTPException(status_code=403, detail="Operation not permitted")
