import datetime, time
import json
import os
from typing import Optional, List

from fastapi import HTTPException
from sqlalchemy import select, text

from core import models
from app.admin import schemas, constants, services
from core.auth import schemas as auth_schemas



""" User """


def get_user(db, role_id):
    users = db.query(models.User).filter(models.User.role_id == role_id, models.User.status == 1).all()

    return users


def get_user_by_role_name(db, role_name):
    role = db.query(models.Role).filter(models.Role.role_name == role_name).first()
    users = db.query(models.User) \
        .filter(models.User.role_id == role.id,
                models.User.status == 1) \
        .all()

    return users


def get_user_by_role_name_v2(db, role_name):
    role = db.query(models.Role).filter(models.Role.role_name == role_name).first()

    if role.role_name == constants.ROLE_GEN:
        gens = get_gens(db, role)
        return gens
    elif role.role_name == constants.ROLE_SUB:
        subs = get_subs(db, role)
        return subs
    elif role.role_name == constants.ROLE_KITCHEN:
        kitchens = get_kitchens(db, role)
        return kitchens
    elif role.role_name == constants.ROLE_DRIVER:
        drivers = get_drivers(db, role)
        return drivers
    elif role.role_name == constants.ROLE_ADMIN:
        admins = get_admins(db, role)
        return admins


def get_gens(db, role: models.Role):
    """
    data parameters:
        photo
        firstname
        lastname
        phone_number
        address
        object
    """
    users = db.query(models.User.id,
                     models.User.photo,
                     models.User.firstname,
                     models.User.lastname,
                     models.User.phone_number,
                     models.User.address,
                     models.Object.name)\
        .join(models.Object, models.User.id == models.Object.gen_contractor_id)\
        .filter(models.User.role_id == role.id,
                models.User.status == 1)\
        .all()
    data = []
    for user in users:
        data.append({
            "user_id": user.id,
            "user_photo": user.photo,
            "user_firstname": user.firstname,
            "user_lastname": user.lastname,
            "user_phone_number": user.phone_number,
            "user_address": user.address,
            "object_name": user.name,
        })

    return data


def get_subs(db, role: models.Role):
    """
    data parameters:
        photo
        firstname
        lastname
        phone_number
        sub_contractor
        object
    """
    users = db.query(models.User.id,
                     models.User.photo,
                     models.Client.firstname,
                     models.Client.lastname,
                     models.User.phone_number,
                     models.User.firstname,
                     models.Object.name) \
        .join(models.ObjectSubContractor, models.User.id == models.ObjectSubContractor.sub_contractor_id)\
        .join(models.Object, models.ObjectSubContractor.object_id == models.Object.id)\
        .join(models.Client, models.User.id == models.Client.sub_contractor_id)\
        .filter(models.User.role_id == role.id,
                models.User.status == 1)\
        .all()

    data = []
    for user in users:
        data.append({
            "user_id": user.id,
            "user_photo": user.photo,
            "client_firstname": user.firstname,
            "client_lastname": user.lastname,
            "user_phone_number": user.phone_number,
            "user_firstname": user.firstname,
            "object_name": user.name,
        })

    return data


def get_kitchens(db, role: models.Role):
    """
    data parameters:
        photo
        firstname
        lastname
        phone_number
        address
    """
    users = db.query(models.User.id,
                     models.User.photo,
                     models.User.firstname,
                     models.User.lastname,
                     models.User.phone_number,
                     models.User.address) \
        .filter(models.User.role_id == role.id,
                models.User.status == 1)\
        .all()

    data = []
    for user in users:
        data.append({
            "user_id": user.id,
            "user_photo": user.photo,
            "user_firstname": user.firstname,
            "user_lastname": user.lastname,
            "user_phone_number": user.phone_number,
            "user_address": user.address
        })

    return data


def get_drivers(db, role: models.Role):
    """
    data:
        photo
        firstname
        lastname
        phone_number
        car_number
    """
    users = db.query(models.User.id,
                     models.User.photo,
                     models.User.firstname,
                     models.User.lastname,
                     models.User.phone_number,
                     models.Car.car_number) \
        .join(models.DriverCar, models.User.id == models.DriverCar.driver_id)\
        .join(models.Car, models.DriverCar.car_id == models.Car.id)\
        .filter(models.User.role_id == role.id,
                models.User.status == 1)\
        .all()

    data = []
    for user in users:
        data.append({
            "user_id": user.id,
            "user_photo": user.photo,
            "user_firstname": user.firstname,
            "user_lastname": user.lastname,
            "user_phone_number": user.phone_number,
            "car_number": user.car_number
        })

    return data


def get_admins(db, role: models.Role):
    """
    data parameters:
        photo
        firstname
        lastname
        phone_number
        address
        object
    """
    users = db.query(models.User.id,
                     models.User.photo,
                     models.User.firstname,
                     models.User.lastname,
                     models.User.phone_number,
                     models.User.address) \
        .filter(models.User.role_id == role.id,
                models.User.status == 1)\
        .all()

    data = []
    for user in users:
        data.append({
            "user_id": user.id,
            "user_photo": user.photo,
            "user_firstname": user.firstname,
            "user_lastname": user.lastname,
            "user_phone_number": user.phone_number,
            "user_address": user.address
        })

    return data


def get_user_by_id(db, id):
    user = db.query(models.User).filter(models.User.id == id, models.User.status == 1).first()

    return user


def create_user(db, data) -> auth_schemas.UserInDb:
    try:
        user_model = models.User(**data)
    except HTTPException:
        raise HTTPException(status_code=500, detail="User has not created")

    db.add(user_model)
    db.commit()
    db.refresh(user_model)
    return user_model


def update_user(db, data):
    try:
        user_model = db.query(models.User).filter(models.User.id == data.id).first()
        user_model.role_id = data.role_id
        user_model.username = data.username
        user_model.password = data.password
        user_model.role_id = data.role_id
        user_model.role_id = data.role_id
    except HTTPException:
        raise HTTPException(status_code=500, detail="User has not created")

    db.add(user_model)
    db.commit()
    db.refresh(user_model)
    return user_model


""" Role """


def get_role(db) -> schemas.Role:
    roles = db.query(models.Role).all()
    return roles


def create_role(db, data) -> schemas.RoleCreate:
    if db.query(models.Role).filter(models.Role.role_name == data.role_name).first():
        raise HTTPException(status_code=403, detail="Role is exist")
    role_model = models.Role(role_name=data.role_name)
    db.add(role_model)
    db.commit()
    db.refresh(role_model)
    return role_model


def update_role(db, data) -> schemas.Role:
    role_model = db.query(models.Role)

    if not role_model.filter(models.Role.id == data.id).first():
        raise HTTPException(status_code=404, detail="Role has not exist")

    if role_model.filter(models.Role.id != data.id, models.Role.role_name == data.role_name).first():
        raise HTTPException(status_code=403, detail="This role name already exists, choose another role name")
    
    role = role_model.filter(models.Role.id == data.id).first()
    role.role_name = data.role_name
    
    db.add(role)
    db.commit()
    db.refresh(role)
    return role


""" Car """


def get_car(db) -> List[schemas.Car]:
    cars = db.query(models.Car).all()

    return cars


def create_car(db, data) -> schemas.Car:
    car = models.Car(
        car_number=data.car_number,
        model=data.model,
        kitchen_id=data.kitchen_id
    )

    db.add(car)
    db.commit()
    db.refresh(car)
    if not car.id:
        raise HTTPException(status_code=500, detail="Car has not successfuly created")
    return car


def update_car(db, data: schemas.Car) -> schemas.Car:
    car_model = db.query(models.Car).filter(models.Car.id == data.id).first()
    car_model.car_number = data.car_number
    car_model.model = data.model
    car_model.kitchen_id = data.kitchen_id

    db.commit()
    db.refresh(car_model)
    return car_model


""" Defect """


def get_defect(db):
    return db.query(models.Defect).all()


# def create_defect(db, data):
#     pass
#
#
# def update_defect(db, data):
#     pass


""" Object """


def get_object(db):
    query_body = text(f"""
                    SELECT
                        objects.id AS object_id,
                        objects.name AS object_name,
                        objects.address AS object_address,
                        (SELECT users.firstname FROM users WHERE id = objects.gen_contractor_id) AS gen,
                        (SELECT users.firstname FROM users WHERE id = objects.kitchen_id) AS kitchen,
                        (SELECT users.firstname FROM users WHERE id = objects.driver_id) AS driver
                    FROM objects;
                    """)
    orders = db.execute(query_body)
    data = []
    for o in orders:
        data.append(o)
    return data


def create_object(db, data) -> schemas.Object:

    """ check object """
    if services.object_exist(db, data.name):
        raise HTTPException(status_code=403, detail="Object already exists")

    """ check gen_contractor """
    if not services.gen_exist(db, data.gen_contractor_id):
        raise HTTPException(status_code=404, detail="Gen contractor has not exists with this gen_contractor_id")

    """ check gen_contractor """
    if not services.kitchen_exist(db, data.kitchen_id):
        raise HTTPException(status_code=404, detail="Kitchen has not exists with this kitchen_id")

    """ check driver """
    if not services.driver_exist(db, data.driver_id):
        raise HTTPException(status_code=404, detail="Driver has not exists with this driver_id")

    """ create """
    obj_model = models.Object(
        name=data.name,
        address=data.address,
        driver_id=data.driver_id,
        gen_contractor_id=data.gen_contractor_id,
        kitchen_id=data.kitchen_id
    )

    try:
        db.add(obj_model)
        db.commit()
        db.refresh(obj_model)
    except HTTPException:
        raise HTTPException(status_code=403, detail="Object has not created")

    return obj_model


def update_object(db, data):
    obj_model = db.query(models.Object).filter(models.Object.id == data.id).first()

    if obj_model:

        """ check gen_contractor """
        if not services.gen_exist(db, data.gen_contractor_id):
            raise HTTPException(status_code=404, detail="Gen contractor has not exists with this gen_contractor_id")

        """ check gen_contractor """
        if not services.kitchen_exist(db, data.kitchen_id):
            raise HTTPException(status_code=404, detail="Kitchen has not exists with this kitchen_id")

        obj_model.name = data.name,
        obj_model.address = data.address,
        obj_model.gen_contractor_id = data.gen_contractor_id,
        obj_model.kitchen_id = data.kitchen_id

        db.add(obj_model)
        db.commit()
        db.refresh(obj_model)
        return obj_model

    raise HTTPException(status_code=404, detail="Object has not exists")


def create_object_sub_group(db, data) -> schemas.ObjectSubGroup:
    if db.query(models.ObjectSubContractor).filter(models.ObjectSubContractor.object_id == data.object_id, models.ObjectSubContractor.sub_contractor_id == data.sub_contractor_id).first():
        raise HTTPException(status_code=500, detail="Data already exist")
    obj_sub_group_model = models.ObjectSubContractor(object_id=data.object_id, sub_contractor_id=data.sub_contractor_id)

    db.add(obj_sub_group_model)
    db.commit()
    db.refresh(obj_sub_group_model)

    return obj_sub_group_model


"""  Client CRUD """


def create_client(db, client):
    client = models.Client(
        phone_number=client.phone_number,
        firstname=client.firstname,
        sub_contractor_id=client.sub_contractor_id,
        lastname=client.lastname
        )

    db.add(client)
    db.commit()
    db.refresh(client)
    if not client.id:
        raise HTTPException(status_code=500, detail="Client has not successfuly created")
    return client


def update_client(db, id: int, data: schemas.ClientCreate) -> schemas.ClientInDb:
    client_model = db.query(models.Client).filter(models.Client.id == id).first()
    client_model.phone_number = data.phone_number
    client_model.firstname = data.firstname
    client_model.lastname = data.lastname

    db.commit()
    db.refresh(client_model)
    return client_model


def get_list(db):
    return db.query(models.Client).filter(models.Client.phone_number != '').all()


"""  Client CRUD End """


""" Order CRUD """


def create_order(db, order: schemas.OrderBody, client: Optional[dict] = None, sub_id=''):
    order_model = models.Order(
        meal_type_id=order.meal_type_id,
        day_time_id=order.day_time_id,
        amount=order.amount,
        client_id=client['id'] if client else 0,
        sub_id=client['sub_contractor_id'] if client else sub_id,
        status=constants.STATUS_ORDERED,
        date=datetime.date.today() + datetime.timedelta(days=1)
    )

    db.add(order_model)
    db.commit()
    db.refresh(order_model)
    return True if order_model.id else False


def create_day_time(db, day_time_data: schemas.DayTimeCreate):
    " check model exist "
    if db.query(models.DayTime).filter(models.DayTime.name == day_time_data.name).first():
        raise HTTPException(status_code=403, detail="DayTime already exist")

    day_time_model = models.DayTime(name=day_time_data.name, active=True)
    db.add(day_time_model)
    db.commit()
    db.refresh(day_time_model)
    return day_time_data


def day_time_list(db):
    return db.query(models.DayTime).filter(models.DayTime.active == True).all()

def meal_type_list(db):
    return db.query(models.MealType).filter(models.MealType.active == True).all()


def create_meal_type(db, meal_type_data: schemas.MealTypeCreate):
    " check model exist "
    if db.query(models.MealType).filter(models.MealType.meal_name == meal_type_data.meal_name).first():
        raise HTTPException(status_code=403, detail="MealType already exist")

    meal_type_model = models.MealType(meal_name=meal_type_data.meal_name, active=True)
    db.add(meal_type_model)
    db.commit()
    db.refresh(meal_type_model)
    return meal_type_model


""" Order CRUD END"""
