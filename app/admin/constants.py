"""
Statuses of order table
"""
STATUS_ORDERED = 0
STATUS_ALLOWED_BY_GEN = 1
STATUS_COMPLETED_BY_KITCHEN = 2
STATUS_SHIPPING = 3
STATUS_COMPLETED = 4
STATUS_CANCELLED = -1

STATUS_DELIVERY_SHIPPING = 0
STATUS_DELIVERY_RECEIVED = 1
STATUS_DELIVERY_CENCELLED = -1

"""
Roles
"""
ROLE_ADMIN = 'admin'
ROLE_GEN = 'gen'
ROLE_SUB = 'sub'
ROLE_KITCHEN = 'kitchen'
ROLE_DRIVER = 'driver'
ROLE_SUB_OFFICE = 'sub_office'


DEFAULT_EMAIL = 'default@gmail.com'
DEFAULT_IMAGE = 'default-avatar.jpg'
DEFAULT_ADDRESS = 'Default address...'


USER_STATUS_ACTIVE = 1
USER_STATUS_DISACTIVE = 0

# {
# 'client_phone': '+998997633029',
#  'orders': [
#      {
#         'meal_type_id': '2',
#         'day_time_id': 1,
#         'amount': 12},
#      {'meal_type_id': '2', 'day_time_id': 2, 'amount': 4},
#      {'meal_type_id': '2', 'day_time_id': 3, 'amount': 75}
#  ]}