import datetime

from fastapi import FastAPI, Depends, HTTPException, status, APIRouter
from sqlalchemy.orm import Session
from typing import List, Optional

from core.database import SessionLocal, engine, Base
from core.auth import schemas as auth_schemas, auth, routes as auth_routes
from core import models
from app.admin.routes import get_db
from app.sub import services


""" Initialisation Router Class """
sub_router = APIRouter()


@sub_router.get('/sub_orders', status_code=200)
def get_sub_orders(date: Optional[datetime.date] = None, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Sub Contractor page
    """

    return services.get_sub_order_list(db, current_user, date)