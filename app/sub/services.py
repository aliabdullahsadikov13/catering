import datetime, time
import json
import os
from typing import List, Optional

from fastapi import HTTPException, Body
from sqlalchemy import text


def get_sub_order_list(db, user, date):

    if not date:
        date = datetime.date.today() + datetime.timedelta(days=1)
    p = []
    query_body = text(f"""
        SELECT
            obj.id as object_id,
            obj.name as object_name,
            users.id as sub_id,
            users.firstname as sub_name,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY

            ) as aup_breakfast,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as aup_lunch,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY

            ) as aup_dinner,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as aup_night,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_breakfast,

            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_lunch,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_dinner,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as foreman_night,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 1
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_breakfast,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 2
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_lunch,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 3
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_dinner,
            (
                SELECT orders.amount
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{date}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 4
                AND orders.sub_id = users.id
                AND objects.id = obj.id
                AND status = 0
                FETCH FIRST ROW ONLY
            ) as worker_night
        FROM ((object_sub_contractors ob_sc
        INNER JOIN objects obj ON ob_sc.object_id = obj.id)
        INNER JOIN users ON ob_sc.sub_contractor_id = users.id)
        WHERE ob_sc.sub_contractor_id = '{user.id}'
        """)
    orders = db.execute(query_body)
    for o in orders:
        p.append(o)

    return p