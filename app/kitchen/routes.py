from fastapi import Depends, HTTPException, APIRouter
from sqlalchemy.orm import Session

from app.admin.role_checker import RoleChecker
from app.kitchen import services
# from app.admin import schemas, contstants
from app.admin import services as admin_services
from app.admin import constants
from core import models
from core.auth import routes as auth_routes
from app.admin.routes import get_db


""" Initialisation Router Class """
kitchen_router = APIRouter()


@kitchen_router.get('/list',
                    status_code=200,
                    dependencies=[Depends(RoleChecker(["admin"]))])
def get_list(db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    Return list of Kitchen
    """
    return services.get_kitchen(db)


@kitchen_router.get("/orders",
                    status_code=200,
                    dependencies=[Depends(RoleChecker(["kitchen"]))])
def get_kitchen_orders_list(db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Kitchen page
    """
    # if not services.user_is_kitchen(current_user):
    #     raise HTTPException(status_code=403, detail="User has not Kitchen permissions")
    return services.get_kitchen_order_list(db, current_user)


@kitchen_router.get("/special/orders",
                    status_code=200,
                    dependencies=[Depends(RoleChecker(["kitchen"]))])
def get_kitchen_orders_list(db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    This order list for Gen Contractor page
    """
    if not services.user_is_kitchen(current_user):
        raise HTTPException(status_code=403, detail="User has not Kitchen permissions")
    return services.get_kitchen_order_list(db, current_user)


@kitchen_router.put('/complete',
                    status_code=200,
                    dependencies=[Depends(RoleChecker(["kitchen"]))])
def get_list(ids: list, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    Return list of Kitchen
    """
    return admin_services.change_order_status(db, ids, constants.STATUS_COMPLETED_BY_KITCHEN)


@kitchen_router.get("/object/details/{id}",
                    status_code=200,
                    dependencies=[Depends(RoleChecker(["kitchen"]))])
def get_details(id: int, db: Session = Depends(get_db), current_user: models.User = Depends(auth_routes.get_current_user)):
    """
    Return sub contractor informations of object
    """
    if not services.user_is_kitchen(current_user):
        raise HTTPException(status_code=403, detail="User has not Kitchen permissions")
    return services.get_subs_of_object(db, id)
