import datetime, time
import json
import os
from typing import List

from fastapi import HTTPException, Body
from sqlalchemy import text

from core import models
from app.admin import constants, schemas as admin_schemas
from app.kitchen import schemas


def get_kitchen(db):
    kitchen_model = db.query(models.User)\
        .filter(models.User.role_id == 3, models.User.status == 1)\
        .all()

    # m = schemas.KitchenList(**kitchen_model)

    return kitchen_model if kitchen_model else {}


def user_is_kitchen(user):
    if (user.role_id == 3) and (user.status == 1):
        return True
    return False


def get_kitchen_objects(db, user_id) -> admin_schemas.Object:
    objs = db.query(models.Object)\
        .filter(models.Object.kitchen_id == user_id)\
        .all()

    return objs


def get_kitchen_order_list_1(db, user):
    kitchen_objects = get_kitchen_objects(db, user.id)
    """
    """
    today = datetime.date.today()
    p = []
    l = {}
    ids = {'id': []}
    for obj in kitchen_objects:
        query_body = text(f"""
                SELECT
                    objects.id AS object_id,
                    objects.name AS object,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_worker
                FROM objects
                WHERE objects.id = '{obj.id}'
                AND objects.kitchen_id = '{user.id}';
                """)
        id_query = text(f"""
                        SELECT orders.id
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND objects.id = '{obj.id}'
                        AND status = 1;
                        """)
        orders = db.execute(query_body)
        id_rows = db.execute(id_query)
        for o in orders:
            l.update(o)
            for id in id_rows:
                ids['id'] += id
            l.update(ids)
        p.append(l)
    return p


def get_kitchen_order_list(db, user):
    # kitchen_objects = get_kitchen_objects(db, user.id)
    today = datetime.date.today()
    p = []
    query_body = text(f"""
        SELECT
            obj.id AS object_id,
            obj.name AS object,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 1
                AND objects.id = obj.id
                AND orders.status = 1
            ) AS breakfast_aup,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 1
                AND objects.id = obj.id
                AND status = 1
            ) AS breakfast_foreman,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 1
                AND objects.id = obj.id
                AND status = 1
            ) AS breakfast_worker,
            (
                SELECT array_agg(orders.id)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND objects.id = obj.id
                AND orders.day_time_id = 1
                AND status = 1
            ) as breakfast_ids,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 2
                AND objects.id = obj.id
                AND status = 1
            ) AS lunch_aup,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 2
                AND objects.id = obj.id
                AND status = 1
            ) AS lunch_foreman,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 2
                AND objects.id = obj.id
                AND status = 1
            ) AS lunch_worker,
            (
                SELECT array_agg(orders.id)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND objects.id = obj.id
                AND orders.day_time_id = 2
                AND status = 1
            ) as lunch_ids,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 3
                AND objects.id = obj.id
                AND status = 1
            ) AS dinner_aup,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 3
                AND objects.id = obj.id
                AND status = 1
            ) AS dinner_foreman,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 3
                AND objects.id = obj.id
                AND status = 1
            ) AS dinner_worker,
            (
                SELECT array_agg(orders.id)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND objects.id = obj.id
                AND orders.day_time_id = 3
                AND status = 1
            ) as dinner_ids,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 1
                AND orders.day_time_id = 4
                AND objects.id = obj.id
                AND status = 1
            ) AS night_aup,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 2
                AND orders.day_time_id = 4
                AND objects.id = obj.id
                AND status = 1
            ) AS night_foreman,
            (
                SELECT COALESCE(SUM(orders.amount), 0)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND orders.meal_type_id = 3
                AND orders.day_time_id = 4
                AND objects.id = obj.id
                AND status = 1
            ) AS night_worker,
            (
                SELECT array_agg(orders.id)
                FROM ((object_sub_contractors
                INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                WHERE orders.date = '{today}'
                AND objects.id = obj.id
                AND orders.day_time_id = 4
                AND status = 1
            ) as night_ids
        FROM objects obj
        WHERE obj.kitchen_id = '{user.id}';
        """)
    orders = db.execute(query_body)
    for o in orders:
        if o['night_ids'] == None and o['dinner_ids'] == None and o['lunch_ids'] == None and o['breakfast_ids'] == None:
            pass
        else:
            p.append(o)
    return p





def get_kitchen_special_order_list(db, user):
    kitchen_objects = get_kitchen_objects(db, user.id)
    """
    """
    today = datetime.date.today() + datetime.timedelta(days=1)
    print(today)
    p = []
    l = {}
    ids = {'id': []}
    for obj in kitchen_objects:
        query_body = text(f"""
                SELECT
                    objects.id AS object_id,
                    objects.name AS object,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 1
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS breakfast_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 2
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS lunch_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 3
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS dinner_worker,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 1
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_aup,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 2
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_foreman,
                    (
                        SELECT SUM(orders.amount)
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND orders.meal_type_id = 3
                        AND orders.day_time_id = 4
                        AND objects.id = '{obj.id}'
                        AND status = 1
                    ) AS night_worker
                FROM objects
                WHERE objects.id = '{obj.id}'
                AND objects.kitchen_id = '{user.id}';
                """)
        id_query = text(f"""
                        SELECT orders.id
                        FROM ((object_sub_contractors
                        INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                        INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                        WHERE orders.date = '{today}'
                        AND objects.id = '{obj.id}'
                        AND status = 1;
                        """)
        orders = db.execute(query_body)
        id_rows = db.execute(id_query)
        for o in orders:
            l.update(o)
            for id in id_rows:
                ids['id'] += id
            l.update(ids)
        p.append(l)
    return p


def get_subs_of_object(db, id):
    today = datetime.date.today()
    sub_query = text(f"""
            SELECT
                objects.id as object_id,
                objects.name as object_name,
                users.id as sub_id,
                users.firstname as sub_name,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                    
                ) as breakfast_aup,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as breakfast_foreman,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 1
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                    
                ) as breakfast_worker,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as lunch_aup,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as lunch_foreman,
                
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 2
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as lunch_worker,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as dinner_aup,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as dinner_foreman,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 3
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as dinner_worker,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 1
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as night_aup,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 2
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as night_foreman,
                (
                    SELECT orders.amount
                    FROM ((object_sub_contractors
                    INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
                    INNER JOIN orders ON object_sub_contractors.sub_contractor_id = orders.sub_id)
                    WHERE orders.date = '{today}'
                    AND orders.meal_type_id = 3
                    AND orders.day_time_id = 4
                    AND orders.sub_id = users.id
                    AND objects.id = '{id}'
                    AND status = 1
                    FETCH FIRST ROW ONLY
                ) as night_worker
            FROM ((object_sub_contractors
            INNER JOIN objects ON object_sub_contractors.object_id = objects.id)
            INNER JOIN users ON object_sub_contractors.sub_contractor_id = users.id)
            WHERE objects.id = '{id}'
            """)

    sub_orders = db.execute(sub_query)

    sub_orders_data = []
    for sub_row in sub_orders:
        sub_orders_data.append(sub_row)

    return sub_orders_data



