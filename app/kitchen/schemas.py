from typing import List, Optional
from pydantic import BaseModel, constr

from core.auth import schemas


"""  Kitchen  """


class KitchenList(BaseModel):
    id: int
    role_id: int
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)
    email: str
    firstname: str
    lastname: str
    address: str
    

