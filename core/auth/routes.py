import os
import shutil
from typing import Optional

import jwt

from datetime import timedelta
from fastapi import Depends, HTTPException, status, APIRouter, Query, Form, UploadFile, File
from pydantic import constr
from sqlalchemy.orm import Session
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm

from core.auth import auth, schemas
from core import models
from app.admin.routes import get_db

auth_router = APIRouter()


# For auth
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="auth/token")


async def get_current_user(db: Session = Depends(get_db), token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, auth.SECRET_KEY, algorithms=[auth.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        token_data = username
    except jwt.PyJWTError:
        raise credentials_exception
    user = auth.get_user(db, username=token_data)
    if user is None:
        raise credentials_exception
    return user


@auth_router.post("/token")
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends(), db: Session = Depends(get_db)):
    """
    This is for auth
    :param db:
    :param form_data:
    :return:
    """
    # print("jello")
    user = auth.authenticate_user(db, form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(hours=auth.ACCESS_TOKEN_EXPIRE_HOUR)
    access_token = auth.create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer", "firstname": user.firstname, "lastname": user.lastname, "role": user.role.role_name}


@auth_router.get("/users/me/")
async def read_users_me(current_user: models.User = Depends(get_current_user)):
    return current_user


@auth_router.patch('/users', status_code=200)
def users_update(data: schemas.UserInDb, db: Session = Depends(get_db)):
    """
    Update User
    :param data:
    :param db:
    :return:
    """
    return auth.update_user(db, data)

#
# @auth_router.post("/users")
# def create_user(user: dict = Depends(schemas.User), db: Session = Depends(get_db)):
#     """
#     Create User
#     :param user: User parameters
#     :param img_file: User photo
#     :param db:
#     :return:
#     """
#     print(user)
#     return auth.create_user(db=db, user=user)


# @auth_router.post("/users")
# def create_user(user: dict, img_file: UploadFile = File(...), db: Session = Depends(get_db)):
#     """
#     Create User
#     :param user: User parameters
#     :param img_file: User photo
#     :param db:
#     :return:
#     """
#     print(user)
#     return auth.create_user(db=db, user=user)

@auth_router.post("/users")
def create_user(
        role: str = Form('admin', enum=(['admin', 'gen', 'sub', 'kitchen', 'driver', 'sub_office']),
                             description='Chooce Role for User, default chooces admin role.'),
        username: str = Form(...),
        password: str = Form(...),
        phone_number: constr(max_length=9, min_length=9, strip_whitespace=True) = Form(...),
        email: str = Form(...),
        status: bool = Form(True, enum=([True, False])),
        firstname: Optional[str] = Form(...),
        lastname: Optional[str] = Form(None),
        address: Optional[str] = Form(None),
        img_file: UploadFile = File(None),
        car: int = Form(...),
        kitchen: int = Form(...),
        db: Session = Depends(get_db)
):
    role = db.query(models.Role).filter(models.Role.role_name == role).first()

    path = os.path.dirname(os.path.abspath("uploads/users/"))
    with open(f'{path + "/users/" + img_file.filename}', "wb") as defect_img:
        shutil.copyfileobj(img_file.file, defect_img)

    user = schemas.User(
        role_id=role.id,
        username=username,
        password=password,
        phone_number=phone_number,
        email=email,
        photo=f"/uploads/users/{img_file.filename.replace(' ', '')}",
        status=status,
        firstname=firstname,
        lastname=lastname,
        address=address,
    )

    return auth.create_user(db=db, user=user)
