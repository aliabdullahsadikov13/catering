from fastapi import HTTPException
from passlib.context import CryptContext
from datetime import datetime, timedelta
from typing import Optional
from sqlalchemy.orm import Session
# from jwt import JWT, AbstractJWKBase
import jwt

from core import models
from core.auth import schemas
from app.admin import services, constants

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


SECRET_KEY = "09d25e094og32s1dg32d1sg6dfs561v6s5d6g13s21sdbfe8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_HOUR = 24


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str):
    return db.query(models.User).filter(models.User.username == username).first()


def create_user(db: Session, user: schemas.User):
    """
    If the new user is not a driver, car_id will be empty
    """
    if services.user_exist(db, user.username):
        raise HTTPException(status_code=403, detail="Username already exists")

    db_user = models.User(
        role_id=user.role_id,
        username=user.username,
        password=get_password_hash(user.password),
        firstname=user.firstname,
        lastname=user.lastname,
        photo=user.photo,
        phone_number=user.phone_number,
        address=user.address,
        email=user.email,
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    """ If new user is driver, we will create relation with driver and car """
    if db_user.id and car_id:
        driver_car_model = models.DriverCar(car_id=car_id, driver_id=db_user.id, status=True)

        db.add(driver_car_model)
        db.commit()
        db.refresh(driver_car_model)

    return db_user


def create_gen_or_kitchen(db: Session, user: schemas.GenOrKitchenCreate, object_id: Optional[int] = None):
    """
    GOAL:
    1 Create User medel as a Gen role
    """
    if services.user_exist(db, user.username):
        raise HTTPException(status_code=403, detail="Username already exists")

    db_user = models.User(
        role_id=user.role_id,
        username=user.username,
        password=get_password_hash(user.password),
        firstname=user.firstname,
        lastname=user.lastname,
        photo=user.photo,
        phone_number=user.phone_number,
        address=user.address,
        email=user.email,
        status=1
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    if object_id:
        """ Update Object Sub model """
        object = db.query(models.Object).filter(models.Object.id == object_id).first()
        if object:
            obj_sub_new = models.ObjectSubContractor(object_id=object_id, sub_contractor_id=db_user.id)
            db.add(obj_sub_new)
            db.commit()
        else:
            raise HTTPException(status_code=404, detail="Object has not found")

    if db_user.id:
        return services.object_as_dict(db_user)
    raise HTTPException(status_code=500, detail="Gen creation failed!")


def update_gen_or_kitchen(db: Session, user: schemas.GenOrKitchenUpdate):
    """
    GOAL:
    1 Update User model as a Gen role
    """
    user_model = db.query(models.User).filter(models.User.id == user.id).first()
    if not user_model:
        raise HTTPException(status_code=404, detail="User not found")

    if services.user_exist(db, user.username, user.id):
        raise HTTPException(status_code=500, detail="This Username is used, please choose another username")

    user_model.username = user.username,
    user_model.password = get_password_hash(user.password),
    user_model.firstname = user.firstname,
    user_model.lastname = user.lastname,
    user_model.phone_number = user.phone_number,
    user_model.photo = user.photo,
    user_model.address = user.address,
    user_model.email = user.email,
    user_model.status = 1,

    db.add(user_model)
    db.commit()
    db.refresh(user_model)

    if user_model.id:
        return services.object_as_dict(user_model)
    raise HTTPException(status_code=500, detail="Gen update failed!")


def create_driver(db: Session, user: schemas.DriverCreate) -> schemas.DriverInDb:
    """
    GOAL:
    1 Create User medel as a Driver role
    2 Create DriverCar model
    """
    if services.user_exist(db, user.username):
        raise HTTPException(status_code=403, detail="Username already exists")

    db_user = models.User(
        role_id=user.role_id,
        username=user.username,
        password=get_password_hash(user.password),
        firstname=user.firstname,
        lastname=user.lastname,
        photo=user.photo,
        phone_number=user.phone_number,
        address=constants.DEFAULT_ADDRESS,
        email=constants.DEFAULT_EMAIL,
        status=1
    )
    db.add(db_user)
    db.commit()
    db.refresh(db_user)

    """ Create relation with driver and car """
    if db_user.id and user.car_id:
        driver_car_model = models.DriverCar(car_id=user.car_id, driver_id=db_user.id, status=True)

        db.add(driver_car_model)
        db.commit()
        db.refresh(driver_car_model)

    car_model = db.query(models.Car).filter(models.Car.id == user.car_id).first()
    data = get_driver(db_user, car_model)

    return data


def update_driver(db: Session, user: schemas.DriverUpdate) -> schemas.DriverInDb:
    """
    GOAL:
    1 Update User model as a Driver role
    2 Update or Create DriverCar model
    """
    user_model = db.query(models.User).filter(models.User.id == user.id).first()
    if not user_model:
        raise HTTPException(status_code=404, detail="User not found")

    if services.user_exist(db, user.username, user.id):
        raise HTTPException(status_code=500, detail="This Username is used, please choose another username")

    user_model.username = user.username,
    user_model.password = get_password_hash(user.password),
    user_model.firstname = user.firstname,
    user_model.lastname = user.lastname,
    user_model.phone_number = user.phone_number,
    user_model.photo = user.photo,
    user_model.address = constants.DEFAULT_ADDRESS,
    user_model.email = constants.DEFAULT_EMAIL,
    user_model.status = 1,

    db.add(user_model)
    db.commit()
    db.refresh(user_model)

    """ Create relation with driver and car """
    if user_model.id and user.car_id:
        driver_car_model = db.query(models.DriverCar).filter(models.DriverCar.driver_id == user_model.id).first()

        if driver_car_model:
            driver_car_model.car_id = user.car_id
        else:
            driver_car_model = models.DriverCar(car_id=user.car_id, driver_id=user_model.id, status=True)

        db.add(driver_car_model)
        db.commit()
        db.refresh(driver_car_model)

    car_model = db.query(models.Car).filter(models.Car.id == user.car_id).first()

    data = get_driver(user_model, car_model)
    return data


def get_driver(user, car):
    data = schemas.DriverInDb(
        id=user.id,
        car_number=car.car_number,
        photo=user.photo,
        username=user.username,
        phone_number=user.phone_number,
        firstname=user.firstname,
        lastname=user.lastname
    )

    return data


def update_user(db: Session, user: schemas.UserInDb, car_id: Optional[int] = None):
    user_model = db.query(models.User).filter(models.User.id == user.id).first()
    if not user_model:
        raise HTTPException(status_code=404, detail="User not found")

    if services.user_exist(db, user.username):
        raise HTTPException(status_code=500, detail="This Username is used, please choose another username")

    user_model.role_id = user.role_id,
    user_model.username = user.username,
    user_model.password = get_password_hash(user.password),
    user_model.firstname = user.firstname,
    user_model.lastname = user.lastname,
    user_model.phone_number = user.phone_number,
    user_model.address = user.address,
    user_model.email = user.email,
    user_model.status = user.status,

    db.add(user_model)
    db.commit()
    db.refresh(user_model)

    """ If new user is driver, we will create relation with driver and car """
    if user_model.id and car_id:
        driver_car_model = db.query(models.DriverCar).filter(models.DriverCar.driver_id == user_model.id).first()

        driver_car_model.car_id = car_id

        db.add(driver_car_model)
        db.commit()
        db.refresh(driver_car_model)

    return user_model


def authenticate_user(db, username: str, password: str):
    user = get_user(db, username)
    if not user:
        return False
    if not verify_password(password, user.password):
        return False
    return user


def create_access_token(data: dict, expires_delta: Optional[timedelta] = None):
    payload = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=60)
    payload.update({"exp": expire})
    encoded_jwt = jwt.encode(payload, SECRET_KEY, ALGORITHM)
    return encoded_jwt

