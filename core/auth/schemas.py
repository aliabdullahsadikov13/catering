from typing import List, Optional, Set
from pydantic import BaseModel, constr
from fastapi.param_functions import Form, Query


class Token(BaseModel):
    access_token: str
    token_type: str


class TokenData(BaseModel):
    username: Optional[str] = None


class User(BaseModel):
    role_id: int
    username: str
    password: str
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)
    email: Optional[str] = None
    status: int
    photo: str
    firstname: Optional[str] = None
    lastname: Optional[str] = None
    address: Optional[str] = None


class UserBase(BaseModel):
    username: str
    phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)
    firstname: Optional[str] = None
    lastname: Optional[str] = None


class GenOrKitchenCreate(UserBase):
    role_id: int
    password: str
    address: str
    email: Optional[str] = None
    photo: str


class GenOrKitchenUpdate(GenOrKitchenCreate):
    id: int


class GenOrKitchenInDb(UserBase):
    id: int
    address: str
    email: Optional[str] = None
    photo: str


class DriverCreate(UserBase):
    role_id: int
    password: str
    car_id: int
    photo: str


class DriverUpdate(UserBase):
    id: int
    password: str
    car_id: int
    photo: str


class DriverInDb(UserBase):
    id: int
    car_number: str
    photo: str


class UserInDb(User):
    id: int

#
# class UserCreate(BaseModel):
#     role_id: int
#     username: str
#     password: str
#     phone_number: constr(max_length=9, min_length=9, strip_whitespace=True)
#     email: str
#     status: int
#     firstname: Optional[str] = None
#     lastname: Optional[str] = None
#     address: Optional[str] = None
