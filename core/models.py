from datetime import datetime, date

from sqlalchemy import Column, ARRAY, ForeignKey, Integer, String, Text, Date, DateTime, Boolean
from sqlalchemy.orm import relationship

from core.database import Base


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    role_id = Column(Integer, ForeignKey("roles.id"))
    username = Column(String, unique=True, index=True)
    password = Column(String)
    email = Column(String, index=True)
    firstname = Column(String, index=True)
    lastname = Column(String, index=True)
    phone_number = Column(String(9))
    status = Column(Integer, index=True, default=1)
    address = Column(Text)
    photo = Column(String, index=True)

    role = relationship("Role")
    clients = relationship("Client", back_populates="sub_contractor")


class Client(Base):
    __tablename__ = 'clients'

    id = Column(Integer, primary_key=True, nullable=False)
    sub_contractor_id = Column(Integer, ForeignKey('users.id'), index=True)
    firstname = Column(String(50), index=True, nullable=False)
    lastname = Column(String(50), index=True, nullable=False)
    phone_number = Column(String(9), index=True, nullable=False, unique=True)

    sub_contractor = relationship("User", back_populates="clients")


class Role(Base):
    __tablename__ = 'roles'

    id = Column(Integer, primary_key=True, index=True)
    role_name = Column(String(50))
    # role = Column(String(50))


class Object(Base):
    __tablename__ = 'objects'

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    address = Column(Text)
    gen_contractor_id = Column(Integer, ForeignKey("users.id"))
    kitchen_id = Column(Integer, ForeignKey("users.id"))
    driver_id = Column(Integer, ForeignKey("users.id"))

    # gen_contractor = relationship("User", back_populates="object")
    # kitchen = relationship("User", back_populates="object")


class ObjectSubContractor(Base):
    __tablename__ = 'object_sub_contractors'

    id = Column(Integer, primary_key=True, index=True)
    object_id = Column(Integer, ForeignKey("objects.id"))
    sub_contractor_id = Column(Integer, ForeignKey("users.id"))


class Order(Base):
    __tablename__ = 'orders'

    id = Column(Integer, primary_key=True, index=True)
    meal_type_id = Column(Integer, ForeignKey("meal_types.id"))
    day_time_id = Column(Integer, ForeignKey("day_times.id"))
    date = Column(String)
    amount = Column(Integer, index=True)
    status = Column(Integer, default=0, index=True)
    client_id = Column(Integer, nullable=False, index=True)
    sub_id = Column(Integer, ForeignKey("users.id"), default=0, index=True)
    driver_id = Column(Integer, ForeignKey("users.id"), index=True)
    created_at = Column(DateTime, default=datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))

    meal_type = relationship("MealType", back_populates="orders")
    day_time = relationship("DayTime", back_populates="orders")


class NewOrder(Base):
    __tablename__ = 'new_orders'

    id = Column(Integer, primary_key=True, index=True)
    date = Column(String)
    status = Column(Integer, default=0, index=True)
    order_data = Column(Text)
    client_id = Column(Integer, nullable=False, index=True)
    sub_id = Column(Integer, ForeignKey("users.id"), default=0, index=True)
    driver_id = Column(Integer, ForeignKey("users.id"), index=True)
    created_at = Column(DateTime, default=datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S"))


class MealType(Base):
    __tablename__ = "meal_types"

    id = Column(Integer, primary_key=True, index=True)
    meal_name = Column(String, index=True)
    meal_name_uz = Column(String, index=True)
    meal_name_ru = Column(String, index=True)
    active = Column(Boolean, default=True)

    orders = relationship("Order", back_populates="meal_type")

class DayTime(Base):
    __tablename__ = "day_times"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    name_uz = Column(String, index=True)
    name_ru = Column(String, index=True)
    active = Column(Boolean, default=True)

    orders = relationship("Order", back_populates="day_time")


class Car(Base):
    __tablename__ = "cars"

    id = Column(Integer, primary_key=True, index=True)
    car_number = Column(String(8), index=True)
    model = Column(String(50), index=True)
    kitchen_id = Column(Integer, ForeignKey("users.id"), index=True)


class DriverCar(Base):
    __tablename__ = "driver_car"

    id = Column(Integer, primary_key=True, index=True)
    car_id = Column(Integer, ForeignKey('cars.id'), index=True)
    driver_id = Column(Integer, ForeignKey('users.id'), index=True)
    status = Column(Boolean, default=True)


class Defect(Base):
    __tablename__ = "defects"

    id = Column(Integer, primary_key=True, index=True)
    delivery_id = Column(Integer, ForeignKey("deliveries.id"), index=True)
    amount = Column(Integer, index=True)
    reason = Column(String(255), index=True)
    comment = Column(Text, index=True)


class DefectReason(Base):
    __tablename__ = "defect_reasons"

    id = Column(Integer, primary_key=True, index=True)
    defect_id = Column(Integer, index=True)
    photo = Column(String(255), index=True)


class Delivery(Base):
    __tablename__ = 'deliveries'

    id = Column(Integer, primary_key=True, index=True)
    date = Column(Date, default=date.today(), index=True)
    driver_id = Column(Integer, ForeignKey('users.id'), index=True)
    object_id = Column(Integer, ForeignKey('objects.id'), index=True)
    car_number = Column(String(8), index=True)
    meal_type = Column(String(50), index=True)
    meal_amounts_json = Column(Text)
    order_ids = Column(ARRAY(String))
    status = Column(Integer, default=0)
    created_at = Column(DateTime, default=datetime.utcnow())
    updated_at = Column(DateTime, default=datetime.utcnow(), onupdate=datetime.utcnow)
