from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:131313ali@localhost:5432/postgres"
# SQLALCHEMY_DATABASE_URL = "postgresql://postgres:131313ali@localhost:5432/cateringdb"
SQLALCHEMY_DATABASE_URL = "postgresql://iface:%ifcae$@10.7.5.109:5432/catering"
# SQLALCHEMY_DATABASE_URL = "mysql+mysqlconnector://root:131313ali@10.7.5.109:3360/TABLES_OF_API"

engine = create_engine(
    SQLALCHEMY_DATABASE_URL,
    pool_size=10,
    max_overflow=20
)

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()
